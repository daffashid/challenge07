import { configureStore } from '@reduxjs/toolkit';
import carsReducer from '../slice/carSlice';
export default configureStore({
  reducer: {
    cars: carsReducer
  }
});