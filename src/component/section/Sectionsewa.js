import { Link } from 'react-router-dom';

function Sectionsewa() {
  return (
    <div >
     {/* <!-- Sewa --> */}
     <div id="sewa" className="container-fluid my-5 px-3">
      <div className="container-lg primar text-light text-center rounded-3 p-5">
        <h2 className="fw-bold justi text-center">Sewa Mobil di (Lokasimu) Sekarang</h2>
        <p className="fz text-light text-center">
           Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.
        </p>
        <Link to="/sewa" className="btn btn-success">Mulai Sewa Mobil</Link>
      </div>
      </div>

    </div>
  );
}

export default Sectionsewa;
