

function Service() {
  return (
    <div >
    <div className="container-lg my-5 pt-5" id="servis">
  <div className="row flex-lg-row d-flex justify-content-center">
    <div className="col-10 col-sm-8 col-lg-6 pe-5">
      <img src="images/img/img_service.png" className="mx-lg-auto img-fluid" alt="Bootstrap Themes" width="auto" height="auto" loading="lazy"/>
    </div>
    <div className="col-lg-6 mb-3 px-4">
      <h3 className="display-5 fw-bold lh-1 ms-auto justi mt-2">Best Car Rental for any kind of trip in your area!</h3>
      <p className="lead fz mt-4">
        Sewa mobil di kawansan sekitarmu bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.
      </p>
      <div>
        <img src="images/img/check.png" className="mx-lg-auto img-fluid" alt="Bootstrap Themes" width="auto" height="auto" loading="lazy"/>
        <p className="d-inline-block ms-1">Sewa Mobil Dengan Supir 12 Jam</p>
      </div>
      <div>
        <img src="images/img/check.png" className="mx-lg-auto img-fluid" alt="Bootstrap Themes" width="auto" height="auto" loading="lazy"/>
        <p className="d-inline-block ms-1">Sewa Mobil Lepas Kunci 24 Jam</p>
      </div>
      <div>
        <img src="images/img/check.png" className="mx-lg-auto img-fluid" alt="Bootstrap Themes" width="auto" height="auto" loading="lazy"/>
        <p className="d-inline-block ms-1">Sewa Mobil Jangka Panjang Bulanan</p>
      </div>
      <div>
        <img src="images/img/check.png" className="mx-lg-auto img-fluid" alt="Bootstrap Themes" width="auto" height="auto" loading="lazy"/>
        <p className="d-inline-block ms-1">Gratis Antar - Jemput Mobil di Bandara</p>
      </div>
      <div>
        <img src="images/img/check.png" className="mx-lg-auto img-fluid" alt="Bootstrap Themes" width="auto" height="auto" loading="lazy"/>
        <p className="d-inline-block ms-1">Layanan Airport Transfer / Drop In Out</p>
      </div>
    </div>
  </div>
</div>      

    </div>
  );
}

export default Service;
