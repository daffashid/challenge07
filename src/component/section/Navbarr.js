import { Link } from 'react-router-dom';

function Navbar() {
  return (
    <div >
     <nav className="navbar navbar-expand-lg fixed-top navbar-light bg-light shadow-lg">
  <div className="container-lg">
  <Link to={`/`}>
      <svg width="100" height="34" viewBox="0 0 100 34" fill="none" xmlns="http://www.w3.org/2000/svg">
        <rect width="100" height="34" fill="#0D28A6"/>
        </svg>
    </Link>
        
          <button className="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="offcanvas offcanvas-end" tabindex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel">
              <div className="offcanvas-header">
                <h5 id="offcanvasRightLabel">Bicar</h5>
                <button type="button" className="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
              </div>
              <div className="offcanvas-body">
                  <ul className="navbar-nav ms-auto mb-lg-0">
                      <li className="nav-item">
                      <a className="nav-link active me-2" href="#servis">Our Service</a>
                      </li>
                      <li className="nav-item">
                      <a className="nav-link active me-2" href="#why">Why Us</a>
                      </li>
                      <li className="nav-item">
                      <a className="nav-link active me-2" href="#testi">Testimonial</a>
                      </li>
                      <li className="nav-item">
                      <a className="nav-link active me-3" href="#faq">FAQ</a>
                      </li>
                      <li className="nav-item">
                      <a className="navbar-link btn btn-success link-light" href="#">Register</a>
                      </li>
                  </ul>
              </div>
            </div>
  </div>
</nav>


    </div>
  );
}

export default Navbar;
