import { Link } from 'react-router-dom';

function Mainc() {
  return (
    <div >
     {/* <!-- MAIN CONTENT--> */}
<div className="container-fluid pt-5 text-dark bg-light">
  <div className="row">
    <div className="col-lg-6 px-4 mt-4 pt-5">
      <h3 className="display-5 fw-bold lh-1 ms-auto justi">Sewa & Rental Mobil Terbaik di Kawasan Sekitarmu!</h3>
        <p className="lead fz">
           Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.
        </p>
        <Link to="/sewa" className="btn btn-success my-2">Mulai Sewa Mobil</Link>
    </div>
    <div className="col-lg-6 mt-4">
      <img src="images/img/img_car.png" className="img-fluid" alt="" width="100%" height="100%"/>
    </div>
  </div>
</div>

    </div>
  );
}

export default Mainc;
