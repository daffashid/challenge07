

function Whyus() {
  return (
    <div >
     <div className="container-fluid" id="why">
  <div className="container-lg my-5 pt-5">
    <div className="row g-3 d-flex justify-content-center">
    <h1 className="fw-bold lh-1 ms-auto justi">Why Us?</h1>
    <p className="lead fz mt-4">
      Mengapa harus pilih Binar Car Rental?      
    </p>
    </div>
    <div className="row g-3 d-flex justify-content-center">
      <div className="col-lg-3 g-3">
        <div className="card shadow-sm p-1">
          <img src="images/img/oke.png" className="img-fluid d-block ms-3 m-1 mt-3" width="32px" height="32px" alt="..."/>
          <div className="card-body">
            <h5>Mobil Lengkap</h5>
            <p className="card-text justi">Tersedia banyak pilihan mobil, kondisi seperti masih baru, bersih dan terawat.</p>
          </div>
        </div>
      </div>

      <div className="col-lg-3 g-3">
        <div className="card shadow-sm p-1">
          <img src="images/img/diskon.png" className="img-fluid d-block ms-3 m-1 mt-3" width="32px" height="32px" alt="..."/>
          <div className="card-body">
            <h5>Harga Murah</h5>
            <p className="card-text justi">Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain.</p>
          </div>
        </div>
      </div>

      <div className="col-lg-3 g-3">
        <div className="card shadow-sm p-1">
          <img src="images/img/time.png" className="img-fluid d-block ms-3 m-1 mt-3" width="32px" height="32px" alt="..."/>
          <div className="card-body">
            <h5>Layanan 24 Jam</h5>
            <p className="card-text justi">Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu.</p>
          </div>
        </div>
      </div>

      <div className="col-lg-3 g-3">
        <div className="card shadow-sm p-1">
          <img src="images/img/juara.png" className="img-fluid d-block ms-3 m-1 mt-3" width="32px" height="32px" alt="..."/>
          <div className="card-body">
            <h5>Sopir Profesional</h5>
            <p className="card-text justi">Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu.</p>
          </div>
        </div>
      </div>

    </div>
  </div>      
</div>

    </div>
  );
}

export default Whyus;
