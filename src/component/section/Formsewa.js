import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { getAllCars, fetchCars } from '../../slice/carSlice';


export default function Formsewa() {

    const [carsFilter, setCarsFilter] = useState([])
    const [capacity, setCapacity] = useState()
    const [waktu, setTime] = useState()
    const [tanggal, setDate] = useState()
    const [driver, setDriver] = useState()

    const dispatch = useDispatch();
    const cars = useSelector(getAllCars);
    const carsStatus = useSelector((state) => state.cars.status);
    const error = useSelector((state) => state.cars.error);

      const filterCars = () =>{
        const filteredCar =  cars.filter(item => {
         let datetime = new Date(tanggal + " " + waktu)
         let date = new Date(item.availableAt);
         let newDate = date.getTime()
     
         const beforeEpochTime = datetime.getTime()
         let filterDate = newDate > beforeEpochTime;
         let filterCapacity = item.capacity <= capacity;
         let filterDriver = item.driver === driver;
         console.log(datetime);
        console.log(date);
        console.log(newDate);
        console.log(newDate, beforeEpochTime);
         return filterCapacity && filterDate && filterDriver ;
        })
        
        console.log(filteredCar);
        setCarsFilter(filteredCar);
       }
    
       useEffect(() => {
        if (carsStatus === 'idle') {
          dispatch(fetchCars());
        }
        console.log("CarsStatus", carsStatus);
      }, [carsStatus, dispatch]);

      let content;
  if (carsStatus === 'loading') {
    content = <div>Loading...</div>;
  } else if (carsStatus === 'succeeded')
{content = (
  <div >
      <div className="container-lg" style={{"padding":"2em", "position": "relative"}}>
      <div className="card" style={{ boxShadow: "0px 0px 10px" }} >
        <div className="row p-3">
          <div className="col-lg-10">
            <div className="row">
             <div className="col-lg-3">
             <label className="form-label" for="driver">Tipe Driver</label>
             <select className="form-select" onInput={(e) => setDriver(Number(e.target.value))}>
                  <option value="1">Dengan Sopir</option>
                  <option value="2">Tanpa Sopir</option>
             </select>
             </div>
              <div className="col-lg-3">
                <label for="tanggal" className="form-label">Tanggal</label>
                <input type="date" className="form-control" id="tanggal" onChange={(e) => setDate((e.target.value))}/>
              </div>
              <div className="col-lg-3">
                <label className="form-label" for="waktu">Waktu Jemput</label>
                <input type="time" id="waktu" className="form-control" placeholder="00:00" onChange={(e) => setTime((e.target.value))} />
              </div>
              <div className="col-lg-3">
                  <label for="penumpang" className="form-label">Jumlah Penumpang</label>
                <input id="penumpang" type="number" className="form-control" placeholder="Jumlah Penumpang" onChange={(e) => setCapacity(Number(e.target.value))} />
              </div>
            </div>
          </div>
          <div className="col-lg-2">
            <div className="row">
              <div className="col-lg pt-2">
                <button id="load-btn" className="btn btn-success text-light mt-4" onClick={filterCars} >Cari Mobil</button>
              </div>
            </div>
          </div>
       </div>
      </div>
      </div>
      
<div id="list-car" className="row pt-5 mt-5">
{carsFilter.map(
    item => {
      return(
        <div class="col-lg-4 d-flex justify-content-center " style = {{"margin-top" : "30px"}}>
            <div class="listcar-card card" style= {{"width" : "25rem"}}>
                <img src={item.image} height="300px" class="card-img-top"  />
                <div class="card-body" style= {{"font-family": "Helvetica"}}>
                    <p> <b>Rp {item.rentPerDay} / hari</b></p>
                    <p>{item.description}</p>
                    <p><img src="./images/img/fi_users.svg" width="16" height="16" /> {item.capacity}</p>
                    <p><img src="./images/img/fi_settings.png" width="16" height="16" /> {item.transmission}</p>
                  <p><img src="./images/img/fi_calendar.svg" width="16" height="16" /> Tahun  {item.year}</p>
                  </div>
                  <div class="card-footer">
                  <button class="btn btn-success col-lg-12">Pilih Mobil</button>
                </div>
                  
            </div>
        </div>
      )
    }

)}
</div>

    </div>
)}else if (carsStatus === 'failed') {
  content = (
    <div>{error}</div>
  )
}

return <>{content}</>

}

