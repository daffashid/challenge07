import { Link } from 'react-router-dom';
import Navbar from './section/Navbarr';
import Mainc from './section/Mainc';
import Service from './section/Service';
import Whyus from './section/Whyus';
import Testi from './section/Testi';
import Sectionsewa from './section/Sectionsewa';
import Faq from './section/Faq';
import Footer from './section/Footer';

function Dashboard() {
  return (
    <div className="App">
     <Navbar />
     <Mainc />
     <Service />
     <Whyus />
     <Testi />
     <Sectionsewa />
     <Faq />
     <Footer />

    </div>
  );
}

export default Dashboard;
