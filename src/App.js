
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Dashboard from './component/Dashboard';
import Sewa from './component/Sewa';

function App() {
  return (
    <BrowserRouter>
        <Routes>
        <Route exact path="/" element={<Dashboard />}>
        </Route>
        <Route path="/sewa" element={<Sewa />}>
        </Route>
        </Routes>
    </BrowserRouter>
  );
}

export default App;
